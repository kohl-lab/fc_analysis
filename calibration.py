import FreezeAnalysis_Functions as Fz
import holoviews as hv
from bokeh.plotting import show

hv.extension('bokeh')


video_dict = {
    'dpath': "/Users/anabottura/University of Glasgow/kohl-lab - RSCABAPP2/Experiments/coho2001_fmcfc1_AB/Calibration/",
    'file': "topview_fmcfc12020-10-19T09_53_55.m4v",
    'fps': 20,
    'cal_sec': 20,
}
stretch = dict(width=1, height=1)

image, crop, video_dict = Fz.LoadAndCrop(video_dict, stretch)
# show(hv.render(image))
hist = Fz.Calibrate(video_dict, cal_pix=10000, SIGMA=1)
# show(hv.render(hist))
