%load_ext autoreload
%autoreload 2
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import fnmatch

# Experiment results to be analysed together
exp_dict={
'directory' : '/Users/anabottura/University of Glasgow/kohl-lab - RSCABAPP2/Experiments/coho2001_fmcfc1_AB/',
'exp_days' : ['Habituation', 'Conditioning', 'Recall'],
'camera' : 'sideview',
}

# Loop through directories and get BatchSummary files to Pd.DataFrames

results_dfs = {}

for session in exp_dict['exp_days']:
    file_d = os.path.normpath(os.path.join(os.path.normpath(exp_dict['directory']), session, exp_dict['camera']))
    if os.path.isdir(file_d):
        filenames = sorted(os.listdir(file_d))
        filenames = fnmatch.filter(filenames, ('*.csv'))
    else:
        raise FileNotFoundError('{path} not found. Check that directory is correct'.format(
            path=file_d))
    results_dfs[session] = pd.DataFrame.from_csv(os.path.join(os.path.normpath(file_d), filenames[0]))