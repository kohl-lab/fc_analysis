FROM continuumio/anaconda3

RUN conda create -y -n ezTrack \
    -c conda-forge python=3.8 pandas=1.3.2 \
    matplotlib=3.1.1 jupyter=1.0.0 \
    holoviews=1.14.5 scipy=1.7.1 scikit-learn=0.24.2 \
    bokeh=2.3.3 jinja2=3.0.3 tqdm

RUN conda init bash && \
    . /root/.bashrc && \
    conda activate ezTrack

RUN conda remove opencv && \
    conda install -c menpo opencv pip

RUN pip install --upgrade pip && \
    pip install opencv-contrib-python

RUN git clone https://github.com/DeniseCaiLab/ezTrack.git && \
    cd ezTrack

CMD ["jupyter", "notebook", "-y", \
    "--no-browser", \
    "--NotebookApp.token", "minian", \
    "--ip", "0.0.0.0", \
    "--port", "8000", \
    "--allow-root"]